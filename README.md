# Contacts

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.2.

## Install  dependencies

Run `npm i`

## Run backend server

Front-end application also requires running back-end server. To run back-end server you can find in README file in back-end repo 

## Development server

Run `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/contacts` directory.


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
