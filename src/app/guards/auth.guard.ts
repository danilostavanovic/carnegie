import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanLoad,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { filter, switchMap, take, tap } from 'rxjs/operators';
import { Store } from '../store/store';

@Injectable()
export class AuthGuard implements CanLoad, CanActivate {
  constructor(private store: Store, private router: Router) {}

  public canLoad(): Observable<boolean> | Promise<boolean> | boolean {
    return this.isUserLogged().pipe(switchMap(() => of(true)));
  }
  public canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this.isUserLogged().pipe(switchMap(() => of(true)));
  }

  private isUserLogged(): Observable<boolean> {
    return this.store.select<boolean>('userIsLogged').pipe(
      tap((userIsLogged) => {
        if (!userIsLogged) {
          this.router.navigate(['/']).then();
        }
      }),
      filter(Boolean),
      take(1)
    );
  }
}
