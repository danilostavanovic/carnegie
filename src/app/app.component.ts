import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject, takeUntil } from 'rxjs';
import { NotificationService } from './services/notification.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  constructor(
    private _snackBar: MatSnackBar,
    private notificationService: NotificationService
  ) {}

  private destroy$: Subject<any> = new Subject<any>();

  ngOnInit(): void {
    this.notificationListener();
  }

  public notificationListener(): void {
    this.notificationService.notifications$
      .pipe(takeUntil(this.destroy$))
      .subscribe({
        next: (messageData) => {
          if (messageData.message && messageData.type) {
            this._snackBar.open(messageData.message, messageData.type);
          }
        },
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}
