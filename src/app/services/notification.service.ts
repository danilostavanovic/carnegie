import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

declare type messageType = 'success' | 'error';

export interface IMessage {
  message: string | null;
  type: messageType | null;
}

const message: IMessage = {
  message: null,
  type: null,
};

@Injectable()
export class NotificationService {
  private notificationSubject$: BehaviorSubject<IMessage> =
    new BehaviorSubject<IMessage>(message);

  public notifications$: Observable<IMessage> = this.notificationSubject$
    .asObservable()
    .pipe(distinctUntilChanged());

  public setNotification(notificationMessage: IMessage): void {
    this.notificationSubject$.next(notificationMessage);
  }
}
