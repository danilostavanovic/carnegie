import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { distinctUntilChanged, pluck } from 'rxjs/operators';
import { AppState } from './app.state';

const state: AppState = {
  user: null,
  contacts: null,
  isLoading: false,
  userIsLogged: false,
  selectedContact: null,
};

@Injectable()
export class Store {
  private subject = new BehaviorSubject<AppState>(state);
  private store: Observable<AppState> = this.subject
    .asObservable()
    .pipe(distinctUntilChanged());

  public select<T>(name: string): Observable<T> {
    return this.store.pipe(pluck(name)) as Observable<T>;
  }

  public setState(dataForState: Partial<AppState>): void {
    Object.entries(dataForState).forEach(([key, value]) => {
      this.set(key, value);
    });
  }

  public set(name: string, changeStateValue: any): void {
    this.subject.next({
      ...this.value,
      [name]: changeStateValue,
    });
  }
  private get value(): AppState {
    return this.subject.value;
  }
}
