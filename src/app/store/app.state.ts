import { IUser } from '../auth/models/user.model';
import { IContact } from '../contacts/models/contact.model';

export interface AppState {
  user: IUser | null;
  contacts: IContact[] | null;
  selectedContact: IContact | null;
  userIsLogged: boolean;
  isLoading: boolean;
}
