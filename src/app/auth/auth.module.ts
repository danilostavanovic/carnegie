import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { SignInContainer } from './containers/sign-in/sign-in.container';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthRoutingModule } from './auth-routing.module';
import { AuthFacade } from './auth.facade';
import { HttpClientModule } from '@angular/common/http';
import { SignUpContainer } from './containers/sign-up/sign-up.container';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { CommonModule } from '@angular/common';
import { AuthApiService } from './api/auth-api.service';

const COMPONENTS = [
  SignInContainer,
  SignUpContainer,
  SignInComponent,
  SignUpComponent,
];
const MODULES = [
  CommonModule,
  ReactiveFormsModule,
  AuthRoutingModule,
  HttpClientModule,
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatIconModule,
  MatProgressSpinnerModule,
];
const PROVIDERS = [AuthFacade, AuthApiService];

@NgModule({
  declarations: [...COMPONENTS],
  providers: [...PROVIDERS],
  imports: [...MODULES],
  exports: [...MODULES],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AuthModule {}
