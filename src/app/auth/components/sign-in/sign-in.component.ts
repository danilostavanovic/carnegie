import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
} from '@angular/core';
import { IUser } from '../../models/user.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-sign-in-component',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SignInComponent {
  @Output()
  public signIn: EventEmitter<Partial<IUser>> = new EventEmitter<
    Partial<IUser>
  >();
  @Output()
  public signIUp: EventEmitter<any> = new EventEmitter<any>();

  public showPassword: boolean = true;

  public passwordType: string = 'password';

  public signInForm: FormGroup = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  public onSignIn(): void {
    if (this.signInForm.valid) {
      const userData: Partial<IUser> = this.signInForm.getRawValue();
      this.signIn.emit(userData);
    }
  }

  public onSignUp(): void {
    this.signIUp.emit();
  }

  public toggleShowPasswordType(): void {
    if (this.signInForm.get('password')?.value !== '') {
      this.showPassword = !this.showPassword;
      this.passwordType = this.showPassword ? 'password' : 'text';
    }
  }
}
