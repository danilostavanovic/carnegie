import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
} from '@angular/core';
import { IUser } from '../../models/user.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sing-up-component',
  templateUrl: './sign-up.component.html',
  styleUrls: ['sign-up.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SignUpComponent {
  constructor(private router: Router) {}

  @Output()
  public signUp: EventEmitter<Partial<IUser>> = new EventEmitter<
    Partial<IUser>
  >();

  public showPassword: boolean = true;

  public passwordType: string = 'password';

  public sighUpForm: FormGroup = new FormGroup({
    fullName: new FormControl('', Validators.required),
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  public onSignUp(): void {
    if (this.sighUpForm.valid) {
      const userData: Partial<IUser> = this.sighUpForm.getRawValue();
      this.signUp.emit(userData);
    }
  }

  public toggleShowPasswordType(): void {
    if (this.sighUpForm.get('password')?.value !== '') {
      this.showPassword = !this.showPassword;
      this.passwordType = this.showPassword ? 'password' : 'text';
    }
  }

  public backToSignIn(): void {
    this.router.navigate(['signIn']).then();
  }
}
