import { Injectable } from '@angular/core';
import { combineLatest, map, Observable } from 'rxjs';
import { Store } from '../store/store';
import { IAuthState } from './state/auth.state';
import { IUser } from './models/user.model';
import { AuthApiService } from './api/auth-api.service';
import { NotificationService } from '../services/notification.service';

@Injectable()
export class AuthFacade {
  constructor(
    public store: Store,
    private authApiService: AuthApiService,
    private notificationService: NotificationService
  ) {}

  public authState$: Observable<IAuthState> = combineLatest([
    this.store.select<IUser>('user'),
    this.store.select<boolean>('userIsLogged'),
    this.store.select<boolean>('isLoading'),
  ]).pipe(
    map(([user, userIsLogged, isLoading]) => {
      return {
        user,
        userIsLogged,
        isLoading,
      };
    })
  );

  public onSinUp(user: Partial<IUser>): void {
    this.store.setState({ isLoading: true });
    this.authApiService.signUpUser(user).subscribe({
      next: () => {
        this.store.setState({
          isLoading: false,
        });
        this.notificationService.setNotification({
          message: 'The account is created successfully',
          type: 'success',
        });
      },
      error: ({ error }) => {
        this.store.setState({
          isLoading: false,
        });
        this.notificationService.setNotification({
          message: error.error,
          type: 'error',
        });
      },
    });
  }

  public onSignIn(user: Partial<IUser>, callback?: () => void): void {
    this.store.setState({ isLoading: true });
    this.authApiService.signInUser(user).subscribe({
      next: (userData) => {
        this.store.setState({
          isLoading: false,
          user: userData,
          userIsLogged: true,
        });
        this.notificationService.setNotification({
          message: `${userData.fullName} successfully logged in`,
          type: 'success',
        });
        if (callback) {
          callback();
        }
      },
      error: ({ error }) => {
        this.store.setState({
          isLoading: false,
        });
        this.notificationService.setNotification({
          message: error.error,
          type: 'error',
        });
      },
    });
  }

  public signOut(callback?: () => void): void {
    this.store.setState({ userIsLogged: false, user: null });
    this.notificationService.setNotification({
      message: `successfully signed out`,
      type: 'success',
    });
    if (callback) {
      callback();
    }
  }
}
