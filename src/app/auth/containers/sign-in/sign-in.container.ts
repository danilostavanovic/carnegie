import { ChangeDetectionStrategy, Component } from '@angular/core';
import { IUser } from '../../models/user.model';
import { Router } from '@angular/router';
import { AuthFacade } from '../../auth.facade';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.container.html',
  styleUrls: ['./sign-in.container.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SignInContainer {
  constructor(public authFacade: AuthFacade, private router: Router) {}

  public onSignIn(user: Partial<IUser>): void {
    this.authFacade.onSignIn(user, () => {
      this.router.navigate(['/contacts']).then();
    });
  }

  public onSignUp(): void {
    this.router.navigate(['signUp']).then();
  }
}
