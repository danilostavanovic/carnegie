import { ChangeDetectionStrategy, Component } from '@angular/core';
import { IUser } from '../../models/user.model';
import { AuthFacade } from '../../auth.facade';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.container.html',
  styleUrls: ['./sign-up.container.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SignUpContainer {
  constructor(public authFacade: AuthFacade) {}

  public onSignUp(userData: Partial<IUser>): void {
    this.authFacade.onSinUp(userData);
  }
}
