import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IUser } from '../models/user.model';
import { Observable } from 'rxjs';

@Injectable()
export class AuthApiService {
  constructor(private http: HttpClient) {}

  private readonly BASE_URL = 'http://localhost:3000/users';

  public signInUser(user: Partial<IUser>): Observable<IUser> {
    return this.http.post<IUser>(`${this.BASE_URL}/auth`, user);
  }

  public signUpUser(user: Partial<IUser>): Observable<IUser> {
    return this.http.post<IUser>(`${this.BASE_URL}/new`, user);
  }
}
