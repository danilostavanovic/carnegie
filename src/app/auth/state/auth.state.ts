import { IUser } from '../models/user.model';
import { AppState } from '../../store/app.state';

export interface IAuthState extends Partial<AppState> {
  user: IUser;
  userIsLogged: boolean;
  isLoading: boolean;
}
