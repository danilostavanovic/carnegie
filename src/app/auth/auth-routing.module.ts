import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignInContainer } from './containers/sign-in/sign-in.container';
import { SignUpContainer } from './containers/sign-up/sign-up.container';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'signIn',
    pathMatch: 'full',
  },
  {
    path: 'signIn',
    component: SignInContainer,
  },
  {
    path: 'signUp',
    component: SignUpContainer,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}
