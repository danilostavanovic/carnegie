import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { IContact } from '../../models/contact.model';

@Component({
  selector: 'app-contact-card-component',
  templateUrl: './contact-card.component.html',
  styleUrls: ['./contact-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactCardComponent {
  @Input()
  public contact?: IContact;

  @Output()
  public deleteContact: EventEmitter<IContact> = new EventEmitter<IContact>();

  @Output()
  public editContact: EventEmitter<IContact> = new EventEmitter<IContact>();
}
