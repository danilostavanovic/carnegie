import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { IUser } from '../../../auth/models/user.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contacts-header',
  templateUrl: './contacts-header.component.html',
  styleUrls: ['./contacts-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactsHeaderComponent {
  constructor(private router: Router) {}

  @Input()
  public title: string = 'My contacts';

  @Input()
  public user: IUser | undefined;

  @Output()
  public signOut: EventEmitter<any> = new EventEmitter<any>();

  public goToList(): void {
    this.router.navigate(['/contacts/list']).then();
  }
}
