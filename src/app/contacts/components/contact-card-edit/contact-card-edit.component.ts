import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
} from '@angular/core';
import { IContact } from '../../models/contact.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact-card-edit',
  templateUrl: './contact-card-edit.component.html',
  styleUrls: ['./contact-card-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactCardEditComponent implements OnInit, OnChanges {
  @Input()
  public contact?: IContact;

  @Output()
  public saveContact: EventEmitter<IContact> = new EventEmitter<IContact>();

  public contactForm: FormGroup = new FormGroup({
    id: new FormControl(null),
    contactName: new FormControl('', Validators.required),
    contactEmail: new FormControl('', [Validators.required, Validators.email]),
    contactAddress: new FormControl('', Validators.required),
  });

  ngOnInit(): void {
    this.patchForm();
  }

  ngOnChanges(): void {
    this.patchForm();
  }

  public onSaveContact(): void {
    if (this.contactForm.valid) {
      const contact: IContact = this.contactForm.getRawValue();
      this.saveContact.emit(contact);
      this.contactForm.reset();
    }
  }

  public onReset(): void {
    this.contactForm.reset();
  }
  private patchForm(): void {
    if (this.contact) {
      this.contactForm.patchValue({
        id: this.contact.id,
        contactName: this.contact.contactName,
        contactEmail: this.contact.contactEmail,
        contactAddress: this.contact.contactAddress,
      });
    }
  }
}
