import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactsApiService } from './api/contacts-api.service';
import { ContactsRoutingModule } from './contacts-routing.module';
import { ContactListContainer } from './containers/contact-list/contact-list.container';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { ContactsHeaderComponent } from './components/contacts-header/contacts-header.component';
import { ContactsFacade } from './contacts.facade';
import { AuthFacade } from '../auth/auth.facade';
import { HttpClientModule } from '@angular/common/http';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ContactCardComponent } from './components/contact-card/contact-card.component';
import { MatCardModule } from '@angular/material/card';
import { ContactContainer } from './containers/contact/contact.container';
import { AuthApiService } from '../auth/api/auth-api.service';
import { ContactNewContainer } from './containers/contact-new/contact-new.container';
import { ContactCardEditComponent } from './components/contact-card-edit/contact-card-edit.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { ContactEditContainer } from './containers/contact-edit/contact-edit.container';

const COMPONENTS = [
  ContactContainer,
  ContactListContainer,
  ContactEditContainer,
  ContactNewContainer,
  ContactCardEditComponent,
  ContactsHeaderComponent,
  ContactCardComponent,
];
const MODULES = [
  CommonModule,
  HttpClientModule,
  ReactiveFormsModule,
  ContactsRoutingModule,
  MatToolbarModule,
  MatIconModule,
  MatButtonModule,
  MatProgressSpinnerModule,
  MatCardModule,
  MatFormFieldModule,
];
const PROVIDERS = [
  ContactsApiService,
  ContactsFacade,
  AuthFacade,
  AuthApiService,
];

@NgModule({
  declarations: [...COMPONENTS],
  providers: [...PROVIDERS],
  imports: [...MODULES, MatInputModule],
  exports: [...MODULES],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ContactsModule {}
