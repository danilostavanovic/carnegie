import { IUser } from '../../auth/models/user.model';
import { IContact } from '../models/contact.model';
import { AppState } from '../../store/app.state';

export interface IContactState extends AppState {
  user: IUser;
  contacts: IContact[];
  userIsLogged: boolean;
  isLoading: boolean;
  selectedContact: IContact;
}
