import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactListContainer } from './containers/contact-list/contact-list.container';
import { ContactContainer } from './containers/contact/contact.container';
import { ContactNewContainer } from './containers/contact-new/contact-new.container';
import { ContactEditContainer } from './containers/contact-edit/contact-edit.container';
import { AuthGuard } from '../guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    canActivate: [AuthGuard],
    redirectTo: '/contacts/list',
  },
  {
    path: '',
    component: ContactContainer,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'list',
        canActivate: [AuthGuard],
        component: ContactListContainer,
      },
      {
        path: 'new',
        canActivate: [AuthGuard],
        component: ContactNewContainer,
      },
      {
        path: 'edit',
        canActivate: [AuthGuard],
        component: ContactEditContainer,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContactsRoutingModule {}
