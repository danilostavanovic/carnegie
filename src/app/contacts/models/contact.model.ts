export interface IContact {
  id: number;
  contactName: string;
  contactEmail: string;
  contactAddress: string;
}
