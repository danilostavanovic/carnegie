import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ContactsFacade } from '../../contacts.facade';
import { Router } from '@angular/router';
import { IContact } from '../../models/contact.model';

@Component({
  selector: 'app-contact-new-container',
  templateUrl: './contact-new.container.html',
  styleUrls: ['./contact-new.container.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactNewContainer {
  constructor(public contactFacade: ContactsFacade, private router: Router) {}

  public goToList(): void {
    this.router.navigate(['/contacts/list']).then();
  }

  public onSaveContact(contact: IContact, userId: number | string): void {
    this.contactFacade.saveNewContact(userId, contact);
  }
}
