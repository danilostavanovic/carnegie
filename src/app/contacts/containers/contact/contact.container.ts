import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ContactsFacade } from '../../contacts.facade';
import { AuthFacade } from '../../../auth/auth.facade';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact-container',
  templateUrl: './contact.container.html',
  styleUrls: ['./contact.container.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactContainer {
  constructor(
    public contactFacade: ContactsFacade,
    public authFacade: AuthFacade,
    private router: Router
  ) {}

  public signOut(): void {
    this.authFacade.signOut(() => {
      this.router.navigate(['/']).then();
    });
  }
}
