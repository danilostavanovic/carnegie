import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ContactsFacade } from '../../contacts.facade';
import { IContact } from '../../models/contact.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact-list-container',
  templateUrl: './contact-list.container.html',
  styleUrls: ['./contact-list.container.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactListContainer implements OnInit {
  constructor(public contactsFacade: ContactsFacade, private router: Router) {}

  ngOnInit(): void {
    this.contactsFacade.getContactsForUser();
  }

  public onDeleteContact(
    contactToDelete: IContact,
    allContacts: IContact[]
  ): void {
    this.contactsFacade.deleteContact(contactToDelete, allContacts);
  }

  public goToNewContact(): void {
    this.router.navigate(['/contacts/new']).then();
  }

  public onEditContact(contact: IContact): void {
    this.contactsFacade.setSelectedContactForEdit(contact, () => {
      this.router.navigate(['/contacts/edit']).then();
    });
  }

  public trackByContactId(index: number, contact: IContact) {
    return contact.id;
  }
}
