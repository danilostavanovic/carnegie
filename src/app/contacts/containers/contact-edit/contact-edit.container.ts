import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ContactsFacade } from '../../contacts.facade';
import { Router } from '@angular/router';
import { IContact } from '../../models/contact.model';

@Component({
  selector: 'app-contact-edit-container',
  templateUrl: './contact-edit.container.html',
  styleUrls: ['./contact-edit.container.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactEditContainer {
  constructor(public contactFacade: ContactsFacade, private router: Router) {}

  public goToList(): void {
    this.router.navigate(['/contacts/list']).then();
  }

  public onEditContact(contact: IContact): void {
    this.contactFacade.updateContact(contact, () => {
      this.router.navigate(['/contacts/list']).then();
    });
  }
}
