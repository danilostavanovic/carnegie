import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IContact } from '../models/contact.model';

@Injectable()
export class ContactsApiService {
  constructor(private http: HttpClient) {}

  private readonly BASE_URL = 'http://localhost:3000/contacts';

  public getContactsFromUser(userId: string | number): Observable<IContact[]> {
    return this.http.get<IContact[]>(`${this.BASE_URL}/${userId}`);
  }

  public saveNewContact(
    userId: string | number,
    contact: IContact
  ): Observable<IContact[]> {
    return this.http.post<IContact[]>(
      `${this.BASE_URL}/new/${userId}`,
      contact
    );
  }

  public updateContact(contact: IContact): Observable<IContact[]> {
    return this.http.put<IContact[]>(`${this.BASE_URL}/update`, contact);
  }

  public deleteContact(contact: IContact): Observable<IContact> {
    return this.http.post<IContact>(`${this.BASE_URL}/delete`, contact);
  }
}
