import { Injectable } from '@angular/core';
import { IContactState } from './state/contact.state';
import { combineLatest, map, Observable, of, switchMap, take } from 'rxjs';
import { Store } from '../store/store';
import { IUser } from '../auth/models/user.model';
import { IContact } from './models/contact.model';
import { ContactsApiService } from './api/contacts-api.service';
import { NotificationService } from '../services/notification.service';

@Injectable()
export class ContactsFacade {
  constructor(
    public store: Store,
    private apiContacts: ContactsApiService,
    private notificationService: NotificationService
  ) {}

  public contactState$: Observable<IContactState> = combineLatest([
    this.store.select<IUser>('user'),
    this.store.select<IContact[]>('contacts'),
    this.store.select<boolean>('userIsLogged'),
    this.store.select<boolean>('isLoading'),
    this.store.select<IContact>('selectedContact'),
  ]).pipe(
    map(([user, contacts, userIsLogged, isLoading, selectedContact]) => {
      return {
        user,
        contacts,
        userIsLogged,
        isLoading,
        selectedContact,
      };
    })
  );

  public getContactsForUser(): void {
    this.store.setState({ isLoading: true });
    this.contactsFromUser$.subscribe({
      next: (contacts) => {
        this.store.setState({ contacts, isLoading: false });
      },
      error: () => {
        this.store.setState({
          isLoading: false,
        });
        this.notificationService.setNotification({
          message: 'ups something went wrong',
          type: 'error',
        });
      },
    });
  }

  private get contactsFromUser$(): Observable<IContact[]> {
    return new Observable((observer) => {
      this.store
        .select<IUser>('user')
        .pipe(
          take(1),
          switchMap((user) =>
            user && user.id
              ? this.apiContacts.getContactsFromUser(user.id)
              : of([])
          )
        )
        .subscribe({
          next: (contact) => {
            observer.next(contact);
            observer.complete();
            observer.unsubscribe();
          },
          error: (e) => {
            observer.next(e);
            observer.complete();
            observer.unsubscribe();
          },
        });
    });
  }

  public setSelectedContactForEdit(
    contact: IContact,
    callback?: () => void
  ): void {
    this.store.setState({ selectedContact: contact });
    if (callback) {
      callback();
    }
  }

  public deleteContact(
    contactToDelete: IContact,
    allContacts: IContact[]
  ): void {
    this.apiContacts.deleteContact(contactToDelete).subscribe({
      next: () => {
        const newContacts = allContacts.filter(
          (contact) => contact.id !== contactToDelete.id
        );
        this.store.setState({ contacts: [...newContacts] });
        this.notificationService.setNotification({
          message: `contact successfully deleted`,
          type: 'success',
        });
      },
      error: () => {
        this.notificationService.setNotification({
          message: `deleting contact failed`,
          type: 'error',
        });
      },
    });
  }

  public saveNewContact(userId: number | string, newContact: IContact): void {
    this.store.setState({ isLoading: true });
    this.apiContacts.saveNewContact(userId, newContact).subscribe({
      next: () => {
        this.store.setState({ isLoading: false });
        this.notificationService.setNotification({
          message: `contact successfully created`,
          type: 'success',
        });
      },
      error: () => {
        this.store.setState({ isLoading: false });
        this.notificationService.setNotification({
          message: `creating new contact failed`,
          type: 'error',
        });
      },
    });
  }

  public updateContact(contact: IContact, callback?: (e?: any) => void): void {
    this.store.setState({ isLoading: true });
    this.apiContacts.updateContact(contact).subscribe({
      next: () => {
        this.store.setState({ isLoading: false });
        this.notificationService.setNotification({
          message: `contact successfully updated`,
          type: 'success',
        });
        if (callback) {
          callback();
        }
      },
      error: (e) => {
        this.store.setState({ isLoading: false });
        this.notificationService.setNotification({
          message: `updating contact failed`,
          type: 'success',
        });
        if (callback) {
          callback(e);
        }
      },
    });
  }
}
